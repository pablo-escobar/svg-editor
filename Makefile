.PHONY: runserver build deploy auto-compile

runserver:
	(cd _site && python3 -m http.server)

build:
	elm make --optimize --output=/tmp/elm-script.js src/Main.elm
	uglifyjs --compress --mangle -o _site/js/script.min.js -- /tmp/elm-script.js
	rm /tmp/elm-script.js
	cp index.html css assets _site/ -r
	cp js/* _site/js/

deploy: build
	rsync -rtv _site/ root@escobar.life:/var/www/svg

auto-compile:
	ls src/*.elm js/* css/* assets/* index.html Makefile | entr make build

