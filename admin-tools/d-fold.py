#!/usr/bin/python3
# A simple utility to fold the `d` attribute of <path> tags without breaking 
# the image

import argparse

def split_at(s, length, sep=" "):
    """Split a string in 'lines' of length ~ `length`"""

    s_lines = [sep + line for line in s.split(sep)]
     
    if not len(s_lines):
        return []

    s_lines[0] = s_lines[0].strip(sep)

    lines = []
    line = ""
    i = 0

    # Construct the lines of the new file
    while i < len(s_lines):
        j = i

        # Construct a single line
        while i < len(s_lines) and len(line + s_lines[i]) <= length:
            line += s_lines[i]
            i += 1

        # Append the line to lines
        if i != j:
            lines.append(line.strip())
            line = ""
        # Line is longer than 50 chars
        else:
            lines.append(s_lines[i].strip())
            i += 1

    return lines

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", type=str, action="store"
                       , default= "-", help="The input file")
    parser.add_argument("--chars", "-n", type=int, default=50, action="store"
                       , help="The number of characters in each line")

    args = parser.parse_args()
    path = args.input if args.input != "-" else "/dev/stdin"

    with open(path, "r") as f:
        s = f.read().replace("\n", "")
        lines = []

        for line in split_at(s, args.chars):
            if len(line) <= args.chars:
                lines.append(line)
            else:
                lines += split_at(line, args.chars, "-")

        print("\n".join(lines))

if __name__ == "__main__":
    main()


