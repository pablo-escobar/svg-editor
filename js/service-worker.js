self.addEventListener('install', installHandler);
self.addEventListener('fetch', fetchHandler);

function installHandler(e) {
    e.waitUntil(
        caches.open('progressive-elm')
            .then((cache) => cache.addAll(['/', '/index.html']))
            .catch(console.error)
    );
}

function fetchHandler(e) {
    e.respondWith(
        caches.match(event.request)
            .then((response) => response || fetch(e.request))
            .catch(console.error)
    );
}
