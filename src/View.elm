module View exposing (view)

import Types exposing (..)
import Editor exposing (editor)

import Html exposing ( Html
                     , Attribute
                     , div
                     , img
                     , button
                     , a
                     , span
                     , text
                     , h1
                     , header
                     )
import Html.Events exposing (onClick, on)
import Html.Attributes exposing ( id
                                , class
                                , src
                                , href
                                , target
                                , rel
                                , alt
                                , attribute
                                , name
                                )
import Html.Lazy exposing (lazy)
import Accessibility.Landmark exposing (application, region)
import Accessibility.Role exposing (alert)
import Accessibility.Key exposing (onKeyDown, enter)
import Accessibility.Widget exposing (label, hidden)
import Browser exposing (Document)
import Json.Decode
import Regex exposing (Regex, Match, replace)


view : Model -> Document Msg
view model =
  { title = "SVG Editor"
  , body =
      case model.load of
        Loading -> 
          []

        Loaded -> 
          [ lazy container model ]
  }

container : Model -> Html Msg
container model =
  div 
    (
      [ id "container", application, label "SVG Editor" ]
      ++ 
      (if valid model.status then [] else [class "error"])
    )
    [ display model
    , lazy editor model
    ]

display : Model -> Html Msg
display model =
  div 
    (
      [ id "display"
      , region
      , class (if model.darkModeOn then "dark" else "light") 
      , label "Picture preview"
      ] 
      ++ 
      -- Alert users when the image is invalid
      (if valid model.status then [] else [ alert ])
    )
    [ img 
      [ id "image", src (uri model), loaded model, alt "Input picture" ] 
      []
    , errorIcon
    , button 
        [ onClick Download
        , label "Download file"
        ] 
        [ downloadIcon ]
    , button 
        [ onClick (Upload Requested)
        , label "Upload file"
        ] 
        [ uploadIcon ]
    , button 
        [ onClick ToggleDarkMode
        , label "Toggle dark mode"
        ] 
        [ bulbIcon model ]
    ]

uri : Model -> String
uri model =
  "data:image/svg+xml;utf8," 
  ++ 
  (replace model.uriEncoder percentEscape model.image)

percentEscape : Match -> String
percentEscape m =
  case m.match of
    "!" -> "%21"
    "#" -> "%23"
    "$" -> "%24"
    "%" -> "%25"
    "&" -> "%26"
    "'" -> "%27"
    "(" -> "%28"
    ")" -> "%29"
    "*" -> "%2A"
    "+" -> "%2B"
    "," -> "%2C"
    "/" -> "%2F"
    ":" -> "%3A"
    ";" -> "%3B"
    "=" -> "%3D"
    "?" -> "%3F"
    "@" -> "%40"
    "[" -> "%5B"
    "]" -> "%5D"
    str -> str

onError : msg -> Attribute msg
onError f = 
  on "error" (Json.Decode.succeed f)

onLoad : msg -> Attribute msg
onLoad f = 
  on "load" (Json.Decode.succeed f)

loaded : Model -> Attribute Msg
loaded model =
  if valid model.status 
    then onError (Validation Invalid) 
    else onLoad (Validation Valid)

valid : Status -> Bool
valid status =
  case status of
    Valid -> 
      True

    Invalid -> 
      False

downloadIcon : Html Msg
downloadIcon = 
  img 
    [ src "assets/download.svg"
    , hidden True
    , alt "Download file"
    ] 
    []

uploadIcon : Html Msg
uploadIcon = 
  img 
    [ src "assets/upload.svg"
    , hidden True
    , alt "Upload file"
    ] 
    []

bulbIcon : Model -> Html Msg
bulbIcon model =
  let
    bulbOn = "assets/bulb-on.svg" 
    bulbOff = "assets/bulb-off.svg"
  in
    img 
      [ src (if model.darkModeOn then bulbOn else bulbOff)
      , hidden True
      , alt "Toggle dark mode"
      ] 
      []

errorIcon : Html Msg
errorIcon = 
  img 
    [ id "error", src "assets/error.svg", alt "Something Went Wrong" ] 
    []

