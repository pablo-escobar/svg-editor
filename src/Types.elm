module Types exposing (..)

import Http
import File exposing (File)
import Regex exposing (Regex)

type alias Model =
  { image : String
  , status : Status
  , darkModeOn : Bool
  , uriEncoder : Regex
  , fileName : String
  , editorScroll : (Int, Int)
  , load : Load
  }

type Status
  = Valid
  | Invalid

type Load
  = Loading
  | Loaded

type Upload
  = Requested
  | Selected File

type Msg
  = Update String
  | Load (Result Http.Error String)
  | Validation Status
  | ToggleDarkMode
  | Download
  | Upload Upload
  | Scroll (Int, Int)

