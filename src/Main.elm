module Main exposing (main)

import View exposing (view)
import Types exposing (..)
import Browser exposing (document)
import Http
import File exposing (File)
import File.Download as Download
import File.Select as Select
import Regex
import Task


main : Program () Model Msg
main =
  document
    { init = \_ -> (init, loadContent)
    , update = update
    , view = view
    , subscriptions = \_ -> Sub.none
    }

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Update image ->
      ({ model | image = image }, Cmd.none)

    Load res ->
      (load model res, Cmd.none)

    Validation val ->
      ({ model | status = val }, Cmd.none)

    ToggleDarkMode ->
      ({ model | darkModeOn = not model.darkModeOn }, Cmd.none)

    Download ->
      (model, Download.string model.fileName "image/svg+xml" model.image)

    Upload upl ->
      upload model upl

    Scroll scroll ->
      ({model | editorScroll = scroll}, Cmd.none)

load : Model -> Result Http.Error String -> Model
load model res =
  case res of
    Ok svg ->
      { model | image = svg, load = Loaded }

    Err _ ->
      { model | load = Loaded }

upload : Model -> Upload -> (Model, Cmd Msg)
upload model upl =
    case upl of
      Requested  ->
        (model, Select.file [ "image/svg+xml" ] (Selected >> Upload))

      Selected file ->
        ( { model | fileName = File.name file }
        , Task.perform Update (File.toString file)
        )

init : Model
init =
  let
      re = "!|#|\\$|%|&|'|\\(|\\)|\\*|\\+|,|\\/|:|;|=|\\?|@|\\[|\\]"
  in
    { image = emptySvg
    , status = Valid
    , darkModeOn = False
    , uriEncoder = Maybe.withDefault Regex.never (Regex.fromString re)
    , fileName = "example.svg"
    , editorScroll = (0, 0)
    , load = Loading
    }

loadContent : Cmd Msg
loadContent =
  Http.get
    { url = "assets/example.svg"
    , expect = Http.expectString Load
    }

emptySvg : String
emptySvg = "<svg> . . . </svg>"

